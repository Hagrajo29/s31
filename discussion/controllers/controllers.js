//containing the functions/statements/logic to be executed once a route has been triggered/entered
//responsible for execution of CRUD operations/methods

//to give access to the contents the for tasks.js file in the models folder; meaning it can use the Task model
const Task = require("../models/task.js");

//create controller dunctions that responds to the routes
module.exports.getAllTasks = () => {
	return Task.find({}).then(result =>{
		return result;
	})
}


module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name:requestBody.name
	})
	//saving mechanisms
	/*
		.then accepts 2 parameters
			-first parameter stores the result object; if we have successfully saved the object
			-second parameter stores the error object, should there be one
	*/
	return newTask.save().then((savedTask,error) => {
		if(error){
			console.log(error);
			return false
		}else{
			return savedTask;
		}
	})
}



module.exports.deleteTask = (taskId) => {
	// findByIdAndRemove - finds the item to be deleted and removes it from the database; it uses id in looking for the document
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if (error){
			console.log(error);
			return false
		}else{
			return removedTask
		}
	})
}


module.exports.updateTask = (taskId,requestBody) => {
	return Task.findById(taskId).then((result,error) => {
		if(error){
			console.log(error)
			return false
		} else {
			result.name = requestBody.name;
			return result.save().then((updateTask,error)=>{
				if (error) {
					console.log(error)
					return false
				}else{
					return updateTask
				}
			})
		}
	})
}


//activity 1. get a specific task in the data base
/*
BUSINESS LOGIC
	"/:id"
	-find the id (findById())
	-return it as a response
*/
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}


//Activity 2. update the status of a task
/*
BUSINESS LOGIC
	"/:id/complete"
	- find the id of the task
	- change the value of the status into "complete" using the requestBody
	- save the task	
*/

module.exports.updateTask = (taskId,requestBody) => {
	return Task.findById(taskId).then((result,error) => {
		if(error){
			console.log(error)
			return false
		} else {
			result.status = requestBody.status;
			return result.save().then((updateTask,error)=>{
				if (error) {
					console.log(error)
					return false
				}else{
					return updateTask
				}
			})
		}
	})
}