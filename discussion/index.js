//set up of dependencies
/*
-load express and mongoose packages and store them inside the "express" and "mongoose" respectively
-create "app" variable and store express server inside
-create a port variavle and store 3000 in it
-make the app able to read json and accept other types of data from forms
-connect your app to mongodb (use the one we have previously used: "b170-to-do" database)
-set up confirmation of connecting or not connecting to the database
-make the app lsiten to port and set up confirmation in the console that the "server is running at port"
*/


const express = require("express");
const mongoose = require("mongoose");

//load the package that is inside the routes.js file in the repo
const taskRoute = require("./routes/routes.js")

const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended:true }));

mongoose.connect("mongodb+srv://Hagrajo9:GwapoKo23@cluster0.rw0wp.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection;
db.on("error", console.error.bind(console,"Connection Error"))
db.once("open", () => console.log("We're connected to the database"))


//gives the app an access to the routes needed
//this will be
app.use("/tasks",taskRoute)













app.listen(port, () => console.log(`Server is running at port ${port}`));
